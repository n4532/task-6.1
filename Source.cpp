#include <iostream>
#include "ImagineNumbers.h"

int main() {
	ImagineNumbers first(5, 6);
	ImagineNumbers second(3, 8);

	std::cout << "Exponentiation: " << std::endl;
	first.exponentiation(2);
	first.displayNumber();
	std::cout << std::endl;

	std::cout << "Multiplication by number: " << std::endl;
	second.multiplicationByNum(3);
	second.displayNumber();
	std::cout << std::endl;

	std::cout << "Sum: " << std::endl;
	ImagineNumbers third = first + second;
	third.displayNumber();
	std::cout << std::endl;

	std::cout << "Subtraction: " << std::endl;
	third = first - second;
	third.displayNumber();
	std::cout << std::endl;

	std::cout << "Multiplication: " << std::endl;
	third = first * second;
	third.displayNumber();
	std::cout << std::endl;

	std::cout << "Division: " << std::endl;
	third = first / second;
	third.displayNumber();
	std::cout << std::endl;

	return 0;
}