#include "ImagineNumbers.h"
#include <iostream>

ImagineNumbers::ImagineNumbers() : ImagineNumbers(1, 1)
{
}

ImagineNumbers::ImagineNumbers(double a, double b)
{
	this->imaginePart = b;
	this->realPart = a;
}

void ImagineNumbers::setImagine(double a)
{
	this->imaginePart = a;
}

void ImagineNumbers::exponentiation(int n)
{
	this->trigonometricForm();

	this->module = pow(this->module, n);
	this->realPart = cos(this->angle * n) * this->module;
	this->imaginePart = sin(this->angle * n) * this->module;
}

void ImagineNumbers::root(int n)
{
	this->trigonometricForm();

	this->module = pow(this->module, 1 / n);
	this->realPart = cos(this->angle * n) / this->module;
	this->imaginePart = sin(this->angle * n) / this->module;
}

void ImagineNumbers::multiplicationByNum(int n)
{
	this->realPart = this->realPart * n;
	this->imaginePart = this->imaginePart * n;
}

void ImagineNumbers::displayNumber()
{
	std::cout << "This imagine number is: " << this->realPart << " + i" << this->imaginePart << std::endl;
}

ImagineNumbers ImagineNumbers::getConjugate()
{
	ImagineNumbers conjugate(this->realPart, -this->imaginePart);
	return conjugate;
}

void ImagineNumbers::trigonometricForm()
{
	this->module = sqrt(pow(this->realPart, 2) + pow(this->imaginePart, 2));
	this->angle = acos(this->realPart / this->module);
}

void ImagineNumbers::setReal(double a)
{
	this->realPart = a;
}

ImagineNumbers operator*(const ImagineNumbers& first, const ImagineNumbers& second)
{
	double real = first.realPart * second.realPart - first.imaginePart * second.imaginePart;
	double imagine = first.realPart * second.imaginePart + second.realPart * first.imaginePart;

	ImagineNumbers result(real, imagine);
	return result;
}

ImagineNumbers operator+(const ImagineNumbers& first, const ImagineNumbers& second)
{
	double real = first.realPart + second.realPart;
	double imagine = first.imaginePart + second.imaginePart;

	ImagineNumbers result(real, imagine);
	return result;
}

ImagineNumbers operator-(const ImagineNumbers& first, const ImagineNumbers& second)
{
	double real = first.realPart - second.realPart;
	double imagine = first.imaginePart - second.imaginePart;

	ImagineNumbers result(real, imagine);
	return result;
}

ImagineNumbers operator/(const ImagineNumbers& first, const ImagineNumbers& second)
{
	double z = pow(second.realPart, 2) + pow(second.imaginePart, 2);

	double real = (first.realPart * second.realPart + first.imaginePart * second.imaginePart) / z;
	double imagine = (first.imaginePart * second.realPart - first.realPart * second.imaginePart) / z;

	ImagineNumbers result(real, imagine);
	return result;
}
