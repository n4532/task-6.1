#pragma once
class ImagineNumbers
{
public:
	ImagineNumbers();
	ImagineNumbers(double, double);
	void setReal(double);
	void setImagine(double);
	void exponentiation(int);
	void root(int);
	void multiplicationByNum(int);
	void displayNumber();
	ImagineNumbers getConjugate();
	friend ImagineNumbers operator * (const ImagineNumbers&, const ImagineNumbers&);
	friend ImagineNumbers operator + (const ImagineNumbers&, const ImagineNumbers&);
	friend ImagineNumbers operator - (const ImagineNumbers&, const ImagineNumbers&);
	friend ImagineNumbers operator / (const ImagineNumbers&, const ImagineNumbers&);

private:
	double imaginePart;
	double realPart;
	double module;
	double angle;

	void trigonometricForm();
};


